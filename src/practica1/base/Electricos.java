package practica1.base;

import java.time.LocalDate;

/**
 * Electricos. Instrumentos de eléctricos, previa selección en los RadioButton que como hijo de la clase instrumento
 * hereda sus atributos, además de otro específico que en este caso es las salidas de audio. Es de naturaleza short
 * porque tiene que ser igual al de resto de instrumentos y el de viento necesita short.
 */
public class Electricos extends Instrumento{
    short Audio;

    public Electricos() {
        super();
    }

    public Electricos(String codigo, String nombre, double pvp, LocalDate fechaAdquisicion, short audio) {
        super(codigo, nombre, pvp, fechaAdquisicion);
        this.Audio = audio;
    }

    public short getAudio() {
        return Audio;
    }

    public void setAudio(short audio) {
        this.Audio = audio;
    }

    @Override
    public String toString() {
        return "Electricos{" +
                "Audio=" + Audio +
                '}';
    }
}
