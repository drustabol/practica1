package practica1.base;

import java.time.LocalDate;

/**
 * Viento. Instrumentos de viento, previa selección en los RadioButton que como hijo de la clase instrumento hereda sus
 * atributos, además de otro específico que en este caso es la longitud en cms. Es de naturaleza short teniendo en
 * cuenta el rango que pueden alcanzar.
 */

public class Viento extends Instrumento{

    private short longitud;

    public Viento(){
        super();
    }

    public Viento(String codigo, String nombre, double pvp, LocalDate fechaAdquisicion, short longitud) {
        super(codigo, nombre, pvp, fechaAdquisicion);
        this.longitud = longitud;
    }

    public short getLongitud() {
        return longitud;
    }

    public void setLongitud(short longitud) {
        this.longitud = longitud;
    }

    @Override
    public String toString() {
        return "Viento{"
                +"código de entrada "+getCodigo()
                +"Nombre del instrumento "+getNombre()
                +"Pvp "+getPvp()
                +'}';
    }
}
