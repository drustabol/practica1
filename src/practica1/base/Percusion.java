package practica1.base;

import java.time.LocalDate;

/**
 * Percusion. Instrumentos de percusión, previa selección en los RadioButton que como hijo de la clase instrumento
 * hereda sus atributos, además de otro específico que en este caso es el peso. Es de naturaleza short porque tiene que
 * ser igual al de resto de instrumentos y el de viento necesita short.
 */
public class Percusion extends Instrumento{

    private short peso;

    public Percusion(){
        super();
    }

    public Percusion(String codigo, String nombre, double pvp, LocalDate fechaAdquisicion, short peso) {
        super(codigo, nombre, pvp, fechaAdquisicion);
        this.peso = peso;
    }

    public short getPeso() {
        return peso;
    }

    public void setPeso(short peso) {
        this.peso = peso;
    }

    @Override
    public String toString() {
        return "Percusion{"
                +"código de entrada "+getCodigo()
                +"Nombre del instrumento "+getNombre()
                +"Pvp "+getPvp()
                +'}';
    }
}
