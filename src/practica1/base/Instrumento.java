package practica1.base;

import java.time.LocalDate;

/**
 * Se define la clase padre(abstract) instrumento. Va a contener todos los atributos comunes a todos los instrumentos:
 * un código de entrada que lo haga único, un nombre, un precio y una fecha de adquisisión. Se crea el constructor y
 * como estos atributos van a ser obtenidos en otras clases, se generan getters y setters,a demás del correspondiente
 * toString.
 */

public abstract class Instrumento {

    private String codigo;
    private String nombre;
    private double pvp;
    private LocalDate fechaAdquisicion;

    public Instrumento(){
        this.codigo = "";
        this.nombre = "";
        this.pvp = 0;
        this.fechaAdquisicion = null;
    }

    public Instrumento(String codigo, String nombre, double pvp, LocalDate fechaAdquisicion) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.pvp = pvp;
        this.fechaAdquisicion = fechaAdquisicion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPvp() {
        return pvp;
    }

    public void setPvp(double pvp) {
        this.pvp = pvp;
    }

    public LocalDate getFechaAdquisicion() {
        return fechaAdquisicion;
    }

    public void setFechaAdquisicion(LocalDate fechaAdquisicion) {
        this.fechaAdquisicion = fechaAdquisicion;
    }
}
