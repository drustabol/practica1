package practica1.base;

import java.time.LocalDate;

/**
 * Cuerda. Instrumentos de cuerda, previa selección en los RadioButton que como hijo de la clase instrumento hereda sus
 * atributos, además de otro específico que en este caso es el número de cuerdas. Es de naturaleza short porque tiene
 * que ser igual al de resto de instrumentos y el de viento necesita short.
 */

public class Cuerda extends Instrumento{
    public short nCuerdas;

    public Cuerda() {
        super();
    }

    public Cuerda(String codigo, String nombre, double pvp, LocalDate fechaAdquisicion, short nCuerdas) {
        super(codigo, nombre, pvp, fechaAdquisicion);
        this.nCuerdas = nCuerdas;
    }

    public short getnCuerdas() {
        return nCuerdas;
    }

    public void setnCuerdas(short nCuerdas) {
        this.nCuerdas = nCuerdas;
    }

    @Override
    public String toString() {
        return "Cuerda{"
                +"código de entrada "+getCodigo()
                +"Nombre del instrumento "+getNombre()
                +"Pvp "+getPvp()
                +'}';
    }
}
