package practica1;
/**
 * Ésta es la clase que da inicio a toda la aplicación. Se crea el objeto de Vista(interfaz gráfica) y las clases que
 * van a permitir la funcionalidad de la aplicación. Nos va a permitir crear una base de datos de distintos tipos de
 * instrumentos.
 */

import practica1.gui.InstrumentosControlador;
import practica1.gui.InstrumentosModelo;
import practica1.gui.Vista;

public class Principal {
    public static void main(String[] args) {
        Vista vista=new Vista();
        InstrumentosModelo modelo=new InstrumentosModelo();
        InstrumentosControlador controlador=new InstrumentosControlador(vista,modelo);
    }


}
