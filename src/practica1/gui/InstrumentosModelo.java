package practica1.gui;

import org.w3c.dom.*;
import org.xml.sax.SAXException;
import practica1.base.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * En esta clase se generan las estructuras de las funciones del alta de los instrumentos, la creación de los ficheros
 * xml donde se exportan todos los instrumentos dados de alta y la estructura de lectura de los ficheros xml cuando haya
 * que importarlos.
 */
public class InstrumentosModelo {
    private ArrayList<Instrumento> listaInstrumentos;

    public InstrumentosModelo() {
       listaInstrumentos=new ArrayList<Instrumento>();
    }

    /**
     * En principo se crea la lista de instrumentos en forma de array, pero no va a ser invocada más que en la clase
     * Instrumentos controlador y devolverá el array de listaInstrumentos
     * @return
     */

    public ArrayList<Instrumento> obtenerInstrumentos(){
        return listaInstrumentos;
    }

    /**
     * Conjunto de métodos por el que se procede a dar de alta cada uno de los instrumentos y son añadidos al array
     * listaInstrumentos, que a su vez, es el que devuelve obtenerInstrumentos en el método de refrescar de
     * la clase InstrumentosControlador.
     */
    public void altaViento (String codigoInstrumento, String nombre, double pvp,
                            LocalDate fechaAdquisicion, short longitud){
        Viento nuevoViento=new Viento(codigoInstrumento,nombre, pvp, fechaAdquisicion,longitud);
        listaInstrumentos.add(nuevoViento);
    }

    public void altaCuerda (String codigoInstrumento, String nombre, double pvp,
                            LocalDate fechaAdquisicion, short cuerdas){
        Cuerda nuevaCuerda = new Cuerda(codigoInstrumento,nombre,pvp,fechaAdquisicion,cuerdas);
        listaInstrumentos.add(nuevaCuerda);
    }

    public void altaPercusion (String codigoInstrumento, String nombre, double pvp,
                               LocalDate fechaAdquisicion, short peso) {
        Percusion nuevaPercusion = new Percusion(codigoInstrumento, nombre, pvp, fechaAdquisicion, peso);
        listaInstrumentos.add(nuevaPercusion);
    }

    public void altaElectricos (String codigoInstrumento, String nombre, double pvp,
                                LocalDate fechaAdquisicion, short audio) {
        Electricos nuevoElectrico = new Electricos(codigoInstrumento, nombre, pvp, fechaAdquisicion, audio);
        listaInstrumentos.add(nuevoElectrico);
    }

    /**
     * Este método nos va a garantizar que no se repita ningún instrumento, comprobando la duplicidad del atributo
     * del código del instrumento, que es el que tiene que ser único para cada uno de ellos.
     */
    public boolean existeCodigo(String codigo) {
        for (Instrumento unInstrumeto: listaInstrumentos) {
            if (unInstrumeto.getCodigo().equalsIgnoreCase(codigo)){
                return true;
            }
        }
        return false;
    }

    /**
     * Método por el que se efectúa la generación del archivo XML que contendrá los instrumentos dados de alta.
     */
    public void exportarXML(File fichero) throws ParserConfigurationException, TransformerException {
        /**
         * Primeramente se requiere del método DocumentBuilderFactory para crear una instancia por medio de un objeto
         * que a su vez, a través de DocumentBuilder y su analizador de fábrica. Se materializa el documento dom por
         * medio de DOMImplementation y posteriormente con el createDocument.
         */
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation dom = builder.getDOMImplementation();
        Document documento = dom.createDocument(null, "xml", null);

        /**
         * Aquí se define la estructura de etiquetas que va a tener el documento. Nodo principal(padre) Instrumentos, y
         * el resto hijos(appendChild) que parten de la raiz
         */

        Element raiz=documento.createElement("Instrumentos");
        documento.getDocumentElement().appendChild(raiz);
        Element nodoInstrumento=null;
        Element nodoDatos=null;
        Text texto=null;

        /**
         * Se recorre todo el ArrayList de Instrumentos(listaInstrumentos) para así determinar el cada uno qué tipo de
         * instrumento es. Para ello se identifican las palabras clave que nos va a dar esa clasificación.
         */
        for (Instrumento unInstrumento:listaInstrumentos) {

            if (unInstrumento instanceof Viento) {
                nodoInstrumento=documento.createElement("Viento");
            }
            if (unInstrumento instanceof Cuerda) {
                nodoInstrumento=documento.createElement("Cuerda");
            }
            if (unInstrumento instanceof Percusion) {
                nodoInstrumento=documento.createElement("Percusion");
            }
            if (unInstrumento instanceof Electricos) {
                nodoInstrumento=documento.createElement("Electricos");
            }
            raiz.appendChild(nodoInstrumento);

            /**
             * Aquí definimos las etiquetas comunes de va a tener cada tipo de instrumento.
             */

            nodoDatos=documento.createElement("codigo-entrada");
            nodoInstrumento.appendChild(nodoDatos);
            texto=documento.createTextNode(unInstrumento.getCodigo());
            nodoDatos.appendChild(texto);

            nodoDatos=documento.createElement("nombre");
            nodoInstrumento.appendChild(nodoDatos);
            texto=documento.createTextNode(unInstrumento.getNombre());
            nodoDatos.appendChild(texto);

            nodoDatos=documento.createElement("pvp");
            nodoInstrumento.appendChild(nodoDatos);
            texto=documento.createTextNode(String.valueOf(unInstrumento.getPvp()));
            nodoDatos.appendChild(texto);

            nodoDatos=documento.createElement("fecha-adquisicion");
            nodoInstrumento.appendChild(nodoDatos);
            texto=documento.createTextNode(unInstrumento.getFechaAdquisicion().toString());
            nodoDatos.appendChild(texto);

            /**
             * Dentro de cada tipo de instrumento se define la etiqueta exclusiva de cada uno de ellos.
             */

            if (unInstrumento instanceof Viento) {
                nodoDatos=documento.createElement("longitud");
                nodoInstrumento.appendChild(nodoDatos);
                texto=documento.createTextNode(String.valueOf(((Viento) unInstrumento).getLongitud()));
                nodoDatos.appendChild(texto);
            }
            if (unInstrumento instanceof Cuerda) {
                nodoDatos=documento.createElement("numero-cuerdas");
                nodoInstrumento.appendChild(nodoDatos);
                texto=documento.createTextNode(String.valueOf(((Cuerda) unInstrumento).getnCuerdas()));
                nodoDatos.appendChild(texto);
            }
            if (unInstrumento instanceof Percusion) {
                nodoDatos=documento.createElement("peso");
                nodoInstrumento.appendChild(nodoDatos);
                texto=documento.createTextNode(String.valueOf(((Percusion) unInstrumento).getPeso()));
                nodoDatos.appendChild(texto);
            }
            if (unInstrumento instanceof Electricos) {
                nodoDatos=documento.createElement("salidas-audio");
                nodoInstrumento.appendChild(nodoDatos);
                texto=documento.createTextNode(String.valueOf(((Electricos) unInstrumento).getAudio()));
                nodoDatos.appendChild(texto);
            }

            /**
             * Se declara el archivo a crear ya con el contenido relleno
             */
            Source source=new DOMSource(documento);
            Result resultado = new StreamResult(fichero);
            /**
             * Se guarda el archivo
             */
            Transformer transformer= TransformerFactory.newInstance().newTransformer();
            transformer.transform(source,resultado);
        }
    }

    public void importarXML(File fichero) throws IOException, SAXException, ParserConfigurationException {

        /**
         * Se declaran tanto el Arraylist de los instrumentos. Por lo que independientemente de si existiera uno o no,
         * se va a generar uno nuevo, eliminando el anterior.
         */
        listaInstrumentos=new ArrayList<Instrumento>();
        Viento nuevoViento=null;
        Cuerda nuevaCuerda=null;
        Percusion nuevoPercusion=null;
        Electricos nuevoElectrico=null;
        /**
         * Mismo principo que en la exportación de un archivo xml, pero como este método es de importación aquí
         * hay que hacer una lectura, de ahí la necesidad de parsear el fichero.
         */
        DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance();
        DocumentBuilder builder=factory.newDocumentBuilder();
        Document documento=builder.parse(fichero);
        /**
         * Se definen los nodos, la estructura jerárquica. Poco a poco por medio de la identificación de las etiquetas
         * se van añadiendo los instrumentos al ArrayList obteniendo cada campo a través del getTextContent de forma
         * jerarquizada
         */
        NodeList listaElementos=documento.getElementsByTagName("*");

        for (int i=0;i<listaElementos.getLength();i++){
            Element nodoInstrumento= (Element) listaElementos.item(i);
            if (nodoInstrumento.getTagName().equals("Viento")) {
                nuevoViento=new Viento();
                nuevoViento.setCodigo(nodoInstrumento.getChildNodes().item(0).getTextContent());
                nuevoViento.setNombre(nodoInstrumento.getChildNodes().item(1).getTextContent());
                nuevoViento.setPvp(Double.parseDouble(nodoInstrumento.getChildNodes().item(2).getTextContent()));
                nuevoViento.setFechaAdquisicion(LocalDate.parse(nodoInstrumento.getChildNodes().item(3).getTextContent()));
                nuevoViento.setLongitud(Short.parseShort(nodoInstrumento.getChildNodes().item(4).getTextContent()));
                listaInstrumentos.add(nuevoViento);
            }

            if (nodoInstrumento.getTagName().equals("Cuerda")) {
                nuevaCuerda=new Cuerda();
                nuevaCuerda.setCodigo(nodoInstrumento.getChildNodes().item(0).getTextContent());
                nuevaCuerda.setNombre(nodoInstrumento.getChildNodes().item(1).getTextContent());
                nuevaCuerda.setPvp(Double.parseDouble(nodoInstrumento.getChildNodes().item(2).getTextContent()));
                nuevaCuerda.setFechaAdquisicion(LocalDate.parse(nodoInstrumento.getChildNodes().item(3).getTextContent()));
                nuevaCuerda.setnCuerdas(Short.parseShort(nodoInstrumento.getChildNodes().item(4).getTextContent()));
                listaInstrumentos.add(nuevaCuerda);
            }

            if (nodoInstrumento.getTagName().equals("Percusion")) {
                nuevoPercusion=new Percusion();
                nuevoPercusion.setCodigo(nodoInstrumento.getChildNodes().item(0).getTextContent());
                nuevoPercusion.setNombre(nodoInstrumento.getChildNodes().item(1).getTextContent());
                nuevoPercusion.setPvp(Double.parseDouble(nodoInstrumento.getChildNodes().item(2).getTextContent()));
                nuevoPercusion.setFechaAdquisicion(LocalDate.parse(nodoInstrumento.getChildNodes().item(3).getTextContent()));
                nuevoPercusion.setPeso(Short.parseShort(nodoInstrumento.getChildNodes().item(4).getTextContent()));
                listaInstrumentos.add(nuevoPercusion);
            }

            if (nodoInstrumento.getTagName().equals("Electricos")) {
                nuevoElectrico=new Electricos();
                nuevoElectrico.setCodigo(nodoInstrumento.getChildNodes().item(0).getTextContent());
                nuevoElectrico.setNombre(nodoInstrumento.getChildNodes().item(1).getTextContent());
                nuevoElectrico.setPvp(Double.parseDouble(nodoInstrumento.getChildNodes().item(2).getTextContent()));
                nuevoElectrico.setFechaAdquisicion(LocalDate.parse(nodoInstrumento.getChildNodes().item(3).getTextContent()));
                nuevoElectrico.setAudio(Short.parseShort(nodoInstrumento.getChildNodes().item(4).getTextContent()));
                listaInstrumentos.add(nuevoElectrico);
            }
        }
    }
}
