package practica1.gui;

import org.xml.sax.SAXException;
import practica1.Util.Util;
import practica1.base.*;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;

/**
 * Esta clase básicamente se va a encargar de asignar las funciones a cada botón, las acciones que se realizan
 * en la interfaz gráfica.
 */
public class InstrumentosControlador implements ActionListener, ListSelectionListener, WindowListener {
    private Vista vista;
    private InstrumentosModelo modelo ;
    private File ultimaRutaExportada;

    public InstrumentosControlador(Vista vista, InstrumentosModelo modelo) {
        this.vista = vista;
        this.modelo = modelo;
        try {
            cargarDatosConfiguracion();
        } catch (IOException e) {
            e.printStackTrace();
        }
        addActionListener(this);
        addListSelectionListener(this);
        addWindowsListener(this);
    }

    /**
     * Es este método se define la finalidad del archivo instrumentos.conf. Aquí se almacena la ruta en la que se quiere
     * almacenar nuestros archivos exportados, para ello se tiene de referencia la última usada. No puede estar vacío
     *
     */
    public void cargarDatosConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.load(new FileReader("Instrumentos.conf"));
        File archivo=new File("Instrumentos.conf");
        if (archivo.length()>0){
            ultimaRutaExportada = new File(configuracion.getProperty("ultimaRutaExportada"));
        }
        else {
            System.out.println("Archivo Intrumentos.conf vacío");
        }

    }

    /**
     * Actualización de la ruta de exportación tras realizar una exportación
     * @param ultimaRutaExportada
     */
    private void actualizaDatosConfiguracion(File ultimaRutaExportada) {
        this.ultimaRutaExportada = ultimaRutaExportada;
    }

    /**
     * Este método se ejecuta para guardar los datos del archivo Intrumento.conf tras cerrar la aplicación por ventana
     * @throws IOException
     */
    private void guardarDatosConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.setProperty("ultimaRutaExportada", ultimaRutaExportada.getAbsolutePath());

        configuracion.store(new PrintWriter("Instrumentos.conf"),
                "Datos confiración instrumentos");
    }

    /**
     * Aquí se define la acción que se realiza tras pulsar los botones de la interffaz gráfica
     *
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();
        switch (actionCommand) {

            /**
             * Creación de instrumento tras hacer comprobación de todos campor rellenos
             *
             */
            case "Nuevo":
                if (hayCamposVacios()) {
                    Util.mensajeError("Los siguientes campos no pueden estar vacíos " +
                            "Código \n Nombre \n Pvp \n Fecha de adquisición" +
                            vista.longitudCuerdasPesoAudioTxt.getText());
                    break;
                }

                if (modelo.existeCodigo(vista.codigoInstrumentoTxt.getText())) {
                    Util.mensajeError("Ya existe un instrumento con ese id\n" +
                            vista.longitudCuerdasPesoAudioTxt.getText());
                    break;
                }

                if (vista.vientoRadioButton.isSelected()) {
                    modelo.altaViento(vista.codigoInstrumentoTxt.getText(),
                            vista.nombreTxt.getText(),
                            Double.parseDouble(vista.pvpTxt.getText()),
                            vista.fechaAdquisicionDPicker.getDate(),
                            Short.parseShort(vista.longitudCuerdasPesoAudioTxt.getText()));
                }
                if (vista.cuerdaRadioButton.isSelected()) {
                    modelo.altaCuerda(vista.codigoInstrumentoTxt.getText(),
                            vista.nombreTxt.getText(),
                            Double.parseDouble(vista.pvpTxt.getText()),
                            vista.fechaAdquisicionDPicker.getDate(),
                            Short.parseShort(vista.longitudCuerdasPesoAudioTxt.getText()));
                }
                if (vista.percusionRadioButton.isSelected()) {
                    modelo.altaPercusion(vista.codigoInstrumentoTxt.getText(),
                            vista.nombreTxt.getText(),
                            Double.parseDouble(vista.pvpTxt.getText()),
                            vista.fechaAdquisicionDPicker.getDate(),
                            Short.parseShort(vista.longitudCuerdasPesoAudioTxt.getText()));
                } else {

                    modelo.altaElectricos(vista.codigoInstrumentoTxt.getText(),
                            vista.nombreTxt.getText(),
                            Double.parseDouble(vista.pvpTxt.getText()),
                            vista.fechaAdquisicionDPicker.getDate(),
                            Short.parseShort(vista.longitudCuerdasPesoAudioTxt.getText()));

                }

                limpiarCampos();
                refrescar();
                break;
            /**
             * De aquí nos lleva al método de importar xml en IntrumentosModelo.
             */
            case "Importar":
                JFileChooser selectorFichero =
                        Util.crearSelectorFichero(ultimaRutaExportada, "Archivos XML", "xml");
                int opt = selectorFichero.showOpenDialog(null);
                if (opt == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.importarXML(selectorFichero.getSelectedFile());
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } catch (SAXException ex) {
                        ex.printStackTrace();
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    }
                    refrescar();
                }
                break;
            /**
             * Este método nos lleva al método de exportar xml en InstrumentosModelo
             */
            case "Exportar":
                JFileChooser selectorFichero2 =
                        Util.crearSelectorFichero(ultimaRutaExportada, "Archivos XML", "xml");
                int opt2 = selectorFichero2.showOpenDialog(null);
                if (opt2 == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.exportarXML(selectorFichero2.getSelectedFile());
                        actualizaDatosConfiguracion(selectorFichero2.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (TransformerException ex) {
                        ex.printStackTrace();
                    }
                }
                break;

            case "Viento":
                vista.longitudCuerdasPesoAudioLbl.setText("Longitud");
                break;

            case "Cuerda":
                vista.longitudCuerdasPesoAudioLbl.setText("Número de cuerdas");
                break;

            case "Percusión":
                vista.longitudCuerdasPesoAudioLbl.setText("Peso");
                break;

            case "Eléctrico":
                vista.longitudCuerdasPesoAudioLbl.setText("Salidas de Audio");
                break;

            case "Salir":
                System.exit(0);
                break;
        }
    }

    /**
     * Aquí se define lo que hace cada radiobutton seleccionado. O sea, determinar si va a ser viento, cuerda, persución
     * o eléctrico
     * @param e
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
            Instrumento instrumentoSeleccionado = (Instrumento) vista.list1.getSelectedValue();
            vista.codigoInstrumentoTxt.setText(instrumentoSeleccionado.getCodigo());
            vista.nombreTxt.setText(instrumentoSeleccionado.getNombre());
            vista.pvpTxt.setText(String.valueOf(instrumentoSeleccionado.getPvp()));
            vista.fechaAdquisicionDPicker.setDate(instrumentoSeleccionado.getFechaAdquisicion());
            if (instrumentoSeleccionado instanceof Viento) {
                vista.vientoRadioButton.doClick();
                vista.longitudCuerdasPesoAudioTxt.setText(String.valueOf(((Viento) instrumentoSeleccionado).getLongitud()));
            }
            if (instrumentoSeleccionado instanceof Cuerda) {
                vista.cuerdaRadioButton.doClick();
                vista.longitudCuerdasPesoAudioTxt.setText(String.valueOf(((Cuerda) instrumentoSeleccionado).getnCuerdas()));
            }

            if (instrumentoSeleccionado instanceof Percusion) {
                vista.percusionRadioButton.doClick();
                vista.longitudCuerdasPesoAudioTxt.setText(String.valueOf(((Percusion) instrumentoSeleccionado).getPeso()));
            } else {
                vista.electricoRadioButton.doClick();
                vista.longitudCuerdasPesoAudioTxt.setText(String.valueOf(((Electricos) instrumentoSeleccionado).getAudio()));
            }
        }
    }

    /**
     * Se da la propiedad de "escuchar" lo que ocurra en cada botón
     */
    private void addActionListener(ActionListener listener){
        vista.vientoRadioButton.addActionListener(listener);
        vista.cuerdaRadioButton.addActionListener(listener);
        vista.percusionRadioButton.addActionListener(listener);
        vista.electricoRadioButton.addActionListener(listener);
        vista.importarButton.addActionListener(listener);
        vista.exportarButton.addActionListener(listener);
        vista.nuevoButton.addActionListener(listener);
        vista.salirButton.addActionListener(listener);
    }

    private void addWindowsListener(WindowListener listener) {
        vista.frame.addWindowListener(listener);
    }

    private void addListSelectionListener(ListSelectionListener listener) {
        vista.list1.addListSelectionListener(listener);
    }

    private void limpiarCampos(){
        vista.longitudCuerdasPesoAudioTxt.setText(null);
        vista.fechaAdquisicionDPicker.setText(null);
        vista.pvpTxt.setText(null);
        vista.nombreTxt.setText(null);
        vista.codigoInstrumentoTxt.setText(null);
        vista.codigoInstrumentoTxt.requestFocus();
    }

    private boolean hayCamposVacios(){
        if (vista.longitudCuerdasPesoAudioTxt.getText().isEmpty() ||
                vista.fechaAdquisicionDPicker.getText().isEmpty() ||
                vista.pvpTxt.getText().isEmpty() ||
                vista.nombreTxt.getText().isEmpty() ||
                vista.codigoInstrumentoTxt.getText().isEmpty()) {
            return true;
        }
        return false;
    }

    public void refrescar(){
        vista.dlmInstrumento.clear();
        for (Instrumento unInstrumento: modelo.obtenerInstrumentos()) {
            vista.dlmInstrumento.addElement(unInstrumento);
        }
    }

    @Override
    public void windowClosing(WindowEvent e) {

        int resp=Util.mensajeConfirmacion("¿Desea cerrar la ventana?","Salir");
        if (resp==JOptionPane.CANCEL_OPTION) {
            try {
                guardarDatosConfiguracion();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            System.exit(0);

        }
    }


    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

}