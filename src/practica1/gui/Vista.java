package practica1.gui;

import com.github.lgooddatepicker.components.DatePicker;
import practica1.base.Instrumento;

import javax.swing.*;
import java.awt.*;

/**
 * Clase de la interfaz gráfica de la aplicación. Se crean los distintos elementos(ventana, seleccionables, campos,
 * etiquetas y botones) para interactuar con ellos. La gran mayoría de atributos de estos elementos se definen en la
 * clase Vista.form en formato xml. Desde los campos, y RadioButtons se rellenan los atributos de los instrumentos.
 */
public class Vista {
    private JPanel panel1;
    public JFrame frame;
    public JRadioButton vientoRadioButton;
    public JRadioButton cuerdaRadioButton;
    public JRadioButton percusionRadioButton;
    public JTextField codigoInstrumentoTxt;
    public DatePicker fechaAdquisicionDPicker;
    public JTextField pvpTxt;
    public JTextField nombreTxt;
    public JButton nuevoButton;
    public JButton importarButton;
    public JButton exportarButton;
    public JRadioButton electricoRadioButton;
    public JLabel longitudCuerdasPesoAudioLbl;
    public JTextField longitudCuerdasPesoAudioTxt;
    public JList list1;
    public JButton salirButton;
    public DefaultListModel<Instrumento> dlmInstrumento;

    public Vista(){
        frame=new JFrame("Instrumentos de música");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        initComponents();
        salirButton.setBackground(new Color(255,14,14));
        salirButton.setForeground(Color.WHITE);
        nuevoButton.setBackground(Color.YELLOW);
        exportarButton.setBackground(Color.BLACK);
        exportarButton.setForeground(Color.WHITE);
        importarButton.setBackground(Color.BLUE);
        importarButton.setForeground(Color.WHITE);
    }

    private void initComponents() {
        dlmInstrumento=new DefaultListModel<Instrumento>();
        list1.setModel(dlmInstrumento);

    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
